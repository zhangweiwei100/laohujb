var gll = {
  //imgUrl:"http://images.guolele.com/item/small/",
  imgUrl:"images/",//本地测试地址
  goback:function() {
    window.history.go(-1);
  },
  //减少数量
  decrement:function(min,target) {
    var num = Number(target.text());
    if(num>min) {
      target.text(num-1);
    }
  },
  //增加数量
  increment:function(max,target) {
    var num = Number(target.text());
    if(num<max) {
      target.text(num+1);
    }
  },
  //计算总价格
  totalPrice:function(quantity,unit) {
    return quantity*unit;
  }  
};

//本地存储类
var localUtils = function() {
  //存储
  this.set = function(name,value) {
    localStorage.setItem(name,value);
  };
  //获取
  this.get = function(name) {
    return localStorage.getItem(name);
  }
};
//购物车类
var cart = function(cartName) {
  var ls = new localUtils();
  this.cartName = cartName;
  //添加商品
  this.add = function(product) {
    var ShoppingCart = ls.get(this.cartName);
        if(ShoppingCart==null||ShoppingCart==""){  
            //第一次加入商品  
            var jsonstr = {"productlist":[{"itemid":product.id,"itemtitle":product.title,"quantity":product.num,"unit":product.unit,"price":product.price,"smallpicurl":product.smallpicurl}]};
            ls.set(this.cartName,JSON.stringify(jsonstr));
        }else{
            var jsonstr = JSON.parse(ShoppingCart);
            var productlist = jsonstr.productlist;  
            var result=false;

            //查找购物车中是否有该商品  
            for(var i in productlist){  
                if(productlist[i].itemid==product.id){  
                    productlist[i].quantity=parseInt(productlist[i].quantity)+parseInt(product.num);
                    productlist[i].price=productlist[i].unit*productlist[i].quantity;
                    result = true;  
                }  
            }  
            if(!result){  
                //没有直接添加  
                productlist.push({"itemid":product.id,"itemtitle":product.title,"quantity":product.num,"unit":product.unit,"price":product.price,"smallpicurl":product.smallpicurl});
            }
            ls.set(this.cartName,JSON.stringify(jsonstr));  
        }  
  };
  //修改商品数量  
    this.update = function(id,num){  
        var ShoppingCart = ls.get(this.cartName);  
        var jsonstr = JSON.parse(ShoppingCart);  
        var productlist = jsonstr.productlist;  
          
        for(var i in productlist){  
            if(productlist[i].itemid==id){  
                productlist[i].quantity=parseInt(num);
                productlist[i].price=productlist[i].unit*productlist[i].quantity;
                ls.set(this.cartName,JSON.stringify(jsonstr));  
                return;  
            }  
        }  
    };
    //获取所有商品  
    this.getList = function(){  
        var ShoppingCart = ls.get(this.cartName);
        if(ShoppingCart!=null) {
          var jsonstr = JSON.parse(ShoppingCart);  
          var productlist = jsonstr.productlist;  
          return productlist;  
        }
    };
    //删除商品  
    this.del = function(id){  
        var ShoppingCart = ls.get(this.cartName);  
        var jsonstr = JSON.parse(ShoppingCart);  
        var productlist = jsonstr.productlist;  
        var list=[];  
        for(var i in productlist){  
            if(productlist[i].itemid!=id){
              list.push(productlist[i]); 
            } 
        }  
        jsonstr.productlist = list; 
        ls.set(this.cartName,JSON.stringify(jsonstr));  
    }  
};
/*
 * 字符串操作StringBuilder
 */
(function(A) {
    A.StringBuilder = function() {
        var C = this;
        var B = [];
        this.append = function(D) {//追加到数组中
            B[B.length] = D
        };
        this.appendLine = function(D) {//追加到数组中，并且每条数据后加换行
            C.append(D);
            C.append("\n")
        };
        this.clear = function() {//清空数组
            B = []
        };
        this.isEmpty = function() {//判断数组是否为空，返回值true或false
            return B.length === 0
        };
        this.toString = function() {//将数组转换为字符串
            return B.join("")
        }
    }
})(window.jQuery || window.Zepto);

/*
 * 格式化金额
 */
function formatNumber(num, pattern) {
    var strarr = num ? num.toString().split('.') : ['0'];
    var fmtarr = pattern ? pattern.split('.') : [''];
    var retstr = '';

    // 整数部分  
    var str = strarr[0];
    var fmt = fmtarr[0];
    var i = str.length - 1;
    var comma = false;
    for (var f = fmt.length - 1; f >= 0; f--) {
        switch (fmt.substr(f, 1)) {
            case '#':
                if (i >= 0) retstr = str.substr(i--, 1) + retstr;
                break;
            case '0':
                if (i >= 0) retstr = str.substr(i--, 1) + retstr;
                else retstr = '0' + retstr;
                break;
            case ',':
                comma = true;
                retstr = ',' + retstr;
                break;
        }
    }
    if (i >= 0) {
        if (comma) {
            var l = str.length;
            for (; i >= 0; i--) {
                retstr = str.substr(i, 1) + retstr;
                if (i > 0 && ((l - i) % 3) == 0) retstr = ',' + retstr;
            }
        }
        else retstr = str.substr(0, i + 1) + retstr;
    }

    retstr = retstr + '.';
    // 处理小数部分  
    str = strarr.length > 1 ? strarr[1] : '';
    fmt = fmtarr.length > 1 ? fmtarr[1] : '';
    i = 0;
    for (var f = 0; f < fmt.length; f++) {
        switch (fmt.substr(f, 1)) {
            case '#':
                if (i < str.length) retstr += str.substr(i++, 1);
                break;
            case '0':
                if (i < str.length) retstr += str.substr(i++, 1);
                else retstr += '0';
                break;
        }
    }
    return retstr.replace(/^,+/, '').replace(/\.$/, '').replace(/\-\,/,"-");
}