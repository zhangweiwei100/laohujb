$(function() {
	var gllcart = new cart("gllCart");
    if(gllcart.getList()!=undefined) {
	    if(gllcart.getList().length>0) {
	      $("#cart_ico").css("bottom","120px");
	    }
    };
	$("#quickMenu").click(function() {
		if($("#btmMenu").hasClass("none")) {
			$("#btmMenu").removeClass("none");
		} else {
			$("#btmMenu").addClass("none");
		}
	});
	//加入购物车
	$(".btnBuy").click(function() {
		if($(this).hasClass("btnBuyDisable")) {
			alert("活动已结束!");
			return false;
		}
        //写入本地存储
        var attr = $(this).attr("data-role").split("|");
        var data = {
            id:attr[0],
            title:attr[1],
            num:attr[2],
            unit:attr[3],
            price:attr[4],
            smallpicurl:attr[5]
        };
        var gllcart = new cart("gllCart");
        gllcart.add(data);

        $("#addSuccess").removeClass("none").animate({opacity:1},300,"ease-out");
        //提示语消失
        setTimeout(function() {
            $("#addSuccess").animate({opacity:0},300,"ease-out",function() {$("#addSuccess").addClass("none")});
        },2000);
        //购物车ico
        $("#cart_ico").css("bottom","120px");
    });
	var tabsSwiper = new Swiper('.swiper-container',{
		speed:500,
		calculateHeight:true,
		onSlideChangeStart: function(){
			$(".sales_tab .active").removeClass('active');
			$(".sales_tab li").eq(tabsSwiper.activeIndex).addClass('active');
		}
	});

	$(".sales_tab li").on('touchstart mousedown',function(e){
		e.preventDefault()
		$(".sales_tab .active").removeClass('active');
		$(this).addClass('active');
		tabsSwiper.swipeTo($(this).index());
	});

})