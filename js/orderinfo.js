$(function () {
	//返回按钮
	$(".prev").click(function () {
		gll.goback();
	});
	//显示购物车
	showCart();
	//点击结算
	$("#add").click(function () {
		if ($("#userid").val() == '') {
			alert("收货地址需要填写清楚!");
			return false;
		}

		if($("input[type='radio'][name='time']:checked").length==0){
			alert("请在页面底部选择配送时间!");
			return false;
		}

		$("#preworder").submit();
	});


})

function showCart() {
	var gllcart = new cart("gllCart");
	var proList = new $.StringBuilder();
	var orderinfo = new $.StringBuilder();
	var products = gllcart.getList();
	//购物车为空的情况下访问确认定页面，将直接跳转到列表页
	if (products == undefined) {
		window.location = "/category/show";
		return;
	}
	var proNum = products.length;
	var totalMoney = 0;
	var freight = Number($("#freight_price").val());
	for (var i = 0; i < proNum; i++) {
		var id = products[i].itemid;
		var price = products[i].price;
		var unit = products[i].unit;
		var quantity = products[i].quantity;
		var title = products[i].itemtitle;
		var smallpicurl = products[i].smallpicurl;

		totalMoney += Number(price);

		proList.append('<li class="pro_list"><div class="product"><div class="fl imgwrap"><img src="' + smallpicurl + '" class="fl"></div><ul><li class="ellipsis"><a href="/fruit/'+id+'">' + title + '</a></li><li>单价：￥' + formatNumber(unit, '#,##0.00') + '</li><li class="tr c666"><span class="fl">数量：' + quantity + '</span><span class="cred">￥<i id="price_' + id + '">' + formatNumber(price, '#,##0.00') + '</i></span></li></ul></div></li>');

		orderinfo.append(id + "," + quantity + "," + unit + ":");
	}
	$("#orderinfo").val(orderinfo.toString());
	$("#orderprice").val(totalMoney);
	$("#proList").html(proList.toString());
	$("#totalMoney,#goodSum").text("￥" + formatNumber(totalMoney, '#,##0.00'));
	$("#freightTxt").text("￥" + formatNumber(freight, '#,##0.00'));
	$("#orderMoney").text("￥" + formatNumber(totalMoney + freight, '#,##0.00'));//应付金额

}