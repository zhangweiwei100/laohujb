var delArr = [];
$(function () {
	showCart();
	//进入编辑模式
	$("#editBtn").click(function () {
		openEditMod();
	});
	//退出编辑模式
	$("#doneBtn").click(function () {
		closeEditMod();
	});
	//结算
	$("#balanceBtn").click(function () {
		if (!$(this).hasClass("btn_disable")) {
			window.location = "/order/preview";
		}
	});
	//删除
	$("#delBtn").click(function () {
		if (delArr.length <= 0) {
			alert("请选择想要删除的商品");
			return false;
		}
		;
		var gllcart = new cart("gllCart");
		for (var i = 0; i < delArr.length; i++) {
			gllcart.del(delArr[i]);
		}
		;
		closeEditMod();
		showCart();
		delArr = [];
	});
	//减少数量
	$("#proList").delegate(".decrement", "click", function () {
		var data = $(this).attr("data").split("|");
		var id = data[0];
		var unit = data[1];
		gll.decrement(1, $("#proNum_" + id));
		var proNum = $("#proNum_" + id).text();
		$("#amount_" + id).text(proNum);
		$("#price_" + id).text(formatNumber(Number(unit * proNum), '#,##0.00'));
	});
	//增加数量
	$("#proList").delegate(".increment", "click", function () {
		var data = $(this).attr("data").split("|");
		var id = data[0];
		var unit = data[1];
		gll.increment(1000, $("#proNum_" + id));
		var proNum = $("#proNum_" + id).text();
		$("#amount_" + id).text(proNum);
		$("#price_" + id).text(formatNumber(Number(unit * proNum), '#,##0.00'));
	});
});
function showCart() {
	var gllcart = new cart("gllCart");
	var proList = new $.StringBuilder();
	var products = gllcart.getList();
	if (products == undefined) {
		$("#proList").html('<li class="noPro tc"><p class="f16 cred">进货单还没有商品，快去选购</p><a href="/category/show" class="btn">去选购</a></li>');
		$("#editBtn,.footer").hide();
		return;
	}
	var proNum = products.length;
	var totalMoney = 0;
	if (proNum <= 0) {
		$("#proList").html('<li class="noPro tc"><p class="f16 cred">进货单还没有商品，快去选购</p><a href="/category/show" class="btn">去选购</a></li>');
		$("#editBtn,.footer").hide();
	} else {
		for (var i = 0; i < proNum; i++) {

			var id = products[i].itemid;
			var price = products[i].price;
			var unit = products[i].unit;
			var quantity = products[i].quantity;
			var title = products[i].itemtitle;
			var smallpicurl = products[i].smallpicurl;

			proList.append('<li class="pro_list"><input type="checkbox" name="cartbox" data="' + id + '|' + price + '" class="abso chk" /><div class="product"><div class="fl imgwrap">' +
			'<img src="' + smallpicurl + '"></div><ul><li class="ellipsis"><a href="/fruit/' + id + '">' + title + '</a></li><li>单价：￥' + formatNumber(unit, '#,##0.00') + '</li>' +
			'<li class="tr c666 mt5"><span class="fl amount">数量：<i id="amount_' + id + '">' + quantity + '</i></span>' +
			'<div class="fl clearfix numberEdit none"><a id="decrement" href="javascript:" class="fl decrement" data="' + id + '|' + unit + '"></a>' +
			'<span id="proNum_' + id + '" class="cred proNum fl">' + quantity + '</span><a href="javascript:" id="increment" class="fl increment" data="' + id + '|' + unit + '"></a></div>' +
			'<span class="cred">￥<i id="price_' + id + '">' + formatNumber(price, '#,##0.00') + '</i></span></li></ul></div></li>');
			totalMoney += Number(price);
		}
		;
		$("#proList").html(proList.toString());
	}
	$("#proNumber").text(proNum);
	$("#totalMoney").text(formatNumber(totalMoney, '#,##0.00'));
	$("input[name='cartbox'],#seleAll").prop("checked", true);
	selectAll("#seleAll", "cartbox");
}
function countPrice() {
	var totalMoney = 0;
	var selected = $("input[name='cartbox']:checked");
	$("#proNumber").text(selected.length);
	for (var i = 0; i < selected.length; i++) {
		totalMoney += Number(selected.eq(i).attr("data").split("|")[1]);
	}
	;
	$("#totalMoney").text(formatNumber(totalMoney, '#,##0.00'));
}
function selectAll(obj, targetName) {
	$(obj).prop("checked", $("input[name='" + targetName + "']").length == $("input[name='" + targetName + "']:checked").length ? true : false);
	//全选按钮
	$(obj).click(function () {
		var unselect = $("input[name='" + targetName + "']");
		unselect.prop("checked", this.checked);
		var selected = $("input[name='" + targetName + "']:checked");
		if (selected.length > 0) {
			$("#delBtn,#balanceBtn").removeClass("btn_disable");
		} else {
			$("#delBtn,#balanceBtn").addClass("btn_disable");
		}
		;
		delArr = [];
		for (var i = 0; i < selected.length; i++) {
			delArr.push(selected.eq(i).attr("data").split("|")[0]);
		}
		;
		countPrice();
	});
	//每个checkbox
	var subBox = $("input[name='" + targetName + "']");
	$(document).delegate(subBox, "click", function () {
		var unselect = $("input[name='" + targetName + "']");
		var selected = $("input[name='" + targetName + "']:checked");
		if (selected.length > 0) {
			$("#delBtn,#balanceBtn").removeClass("btn_disable");
		} else {
			$("#delBtn,#balanceBtn").addClass("btn_disable");
		}
		;
		delArr = [];
		for (var i = 0; i < selected.length; i++) {
			delArr.push(selected.eq(i).attr("data").split("|")[0]);
		}
		;
		$(obj).prop("checked", (unselect.length == selected.length) ? true : false);
		countPrice();
	});
}
function openEditMod() {
	$(".wrap").addClass("editMod");
	$("#doneBtn,.numberEdit").removeClass("none");//显示完成按钮
	$("#editBtn").hide();//隐藏编辑按钮
	$("#cartTotal,#balanceBtn").hide();//隐藏购物车总价格和结算按钮
	$("#delBtn").show().addClass("btn_disable");//显示删除按钮
	$("input[name='cartbox'],#seleAll").prop("checked", false);
	$(".amount").addClass("none");
}
function closeEditMod() {
	$(".wrap").removeClass("editMod");
	$("#doneBtn,.numberEdit").addClass("none");//隐藏完成按钮
	$("#editBtn").show();//显示编辑按钮
	$("#cartTotal,#balanceBtn").show();//显示购物车总价格和结算按钮
	$("#delBtn").hide();//隐藏删除按钮
	$("#balanceBtn").removeClass("btn_disable");
	$(".amount").removeClass("none");
	var gllcart = new cart("gllCart");
	var products = gllcart.getList();
	for (var i = 0; i < products.length; i++) {
		var id = products[i].itemid;
		var num = $("#proNum_" + id).text();
		gllcart.update(id, num);
	}
	;
	showCart();
}