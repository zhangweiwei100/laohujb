$(function(){
  $(".mask").css("height",$(document).height());
  var gllcart = new cart("gllCart");
  if(gllcart.getList()!=undefined) {
    if(gllcart.getList().length>0) {
      $("#cart_ico").css("bottom","50px");
    }
  };
  var imgs = $("#imgs li");//获取焦点图数量
  var num = "";
  //生成小圆点
  for(var i=0;i<imgs.length;i++) {
    num+="<li>"+(i+1)+"</li>"
  };
  $("#nums").html(num);
  var bullets =$("#nums li");
  bullets.eq(0).addClass("on");
  //焦点图插件
  var slider = Swipe(document.getElementById('slider'), {
    auto: 3000,
    continuous: true,
    callback: function(pos) {
      var i = bullets.length;
      while (i--) {
        bullets[i].className = '';
      }
      bullets[pos].className = 'on';
    }
  });

  //返回按钮
  $(".prev").click(function() {
    gll.goback();
  });

  //添加到购物车
  $("#add").click(function() {
    if($(this).hasClass("btnDisable")) {
      return false;
    }
    $("#addBox").removeClass("none");
  });

  //关闭加入购物车弹层
  $("#closeBox").click(function() {
    hideAddBox();
  });

  $("#btnAdd").click(function() {
    //写入本地存储
    var data = {
      id:$("#itemid").val(),
      title:$("#itemtitle").val(),
      num:$("#quantity").val(),
      unit:$("#unit").val(),
      price:$("#price").val(),
      smallpicurl:$("#smallpicurl").val()
    };
    var gllcart = new cart("gllCart");
    gllcart.add(data);

    $("#addSuccess").removeClass("none").animate({opacity:1},300,"ease-out");
    hideAddBox();
    //提示语消失
    setTimeout(function() {
       $("#addSuccess").animate({opacity:0},300,"ease-out",function() {$("#addSuccess").addClass("none")});
    },2000);
    //购物车ico
    $("#cart_ico").css("bottom","50px");
  });
  //立即购买
  $("#promptlyBuy").click(function() {
    var data = {
      id:$("#itemid").val(),
      title:$("#itemtitle").val(),
      num:$("#quantity").val(),
      unit:$("#unit").val(),
      price:$("#price").val(),
      smallpicurl:$("#smallpicurl").val()
    };
    var gllcart = new cart("gllCart");
    gllcart.add(data);
    window.location="cart.html";
  });
  //减少数量
  $("#decrement").click(function() {
    gll.decrement(1,$("#proNum"));
    $("#quantity").val($("#proNum").text());
    countPrice();
  });

  //增加数量
  $("#increment").click(function() {
    gll.increment(100,$("#proNum"));
    $("#quantity").val($("#proNum").text());
    countPrice();
  });
});
function hideAddBox() {
  var _this = $("#addBox");
    _this.addClass("none")
}
//计算总价格
function countPrice() {
  var unit = $("#unit").val();
  var quantity = $("#quantity").val();
  var price = gll.totalPrice(quantity,unit);
  console.log(price);
  $("#price").val(price);
  $("#totalPrice").text(price);
}